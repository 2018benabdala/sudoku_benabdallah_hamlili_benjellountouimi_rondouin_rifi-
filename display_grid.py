from tkinter import *

def graphical_sudoku_init():
    grandes_cases=[]
    root = Tk()
    root.title("sudoku")
    top=Toplevel(root)
    background = Frame(top)
    background.grid(row=0,column=0)
    for i in range(3):
        grandes_cases.append([])
        for j in range(3):
            grandes_cases[i].append(Frame(background, bg="#eee4da", height=180, width=180,bd=2,relief='groove'))
            grandes_cases[i][j].grid(row=i,column=j)
            petites_cases=[]
            for k in range(3):
                petites_cases.append([])
                for l in range(3):
                    petites_cases[k].append(Frame(grandes_cases[i][j],bg="#eee4da", height=60, width=60,bd=1,relief='groove' ))
                    petites_cases[k][l].grid(row=k,column=l)
    root.mainloop()


