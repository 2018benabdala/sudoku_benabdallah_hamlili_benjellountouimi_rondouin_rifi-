#Afficher la grille

def affiche(g):
    x='\n'+'-'*13
    for i in range(9):
        x+= '\n'+'|'
        for j in range(9):
            if g[i][j]==0:
                x+='.'
            if g[i][j]!=0 :
                x+=str(g[i][j])
            if j%3==2 :
                x+='|' # si j correspond à la fin d'une mini-case
        if i%3==2 :
            x+='\n'+'-'*13 #si i correspond a la fin d'un bloc
    return x


#Reconnaitre les positions remplies de la grille
def positions_initiales(grille):
    n=len(grille)
    l=[]
    for i in range(n):
        for j in range(n):
            if grille[i][j]!=0:
                l.append([i,j])
    return (l)

# Modifier la grille en fonction de l'entree de l'utilisateur
def play_sudoku_etape1(grille):
    ligne0 = input("Entrez le numéro de la ligne de la case à modifier : ")
    colonne0 = input("Entrez le numéro de la colonne de la case à modifier : ")
    ligne=int(ligne0)
    colonne=int(colonne0)
    while ligne<1 or ligne>9 or colonne<1 or colonne>9 or [ligne-1,colonne-1] in positions_initiales(grille):
        print("Coordonnées non valides")
        ligne0 = input("Entrez le numéro de la ligne de la case à modifier : ")
        colonne0 = input("Entrez le numéro de la colonne de la case à modifier : ")
        ligne=int(ligne0)
        colonne=int(colonne0)
    numero = int(input("Entrez votre chiffre"))
    while numero<1 or numero>9:
         numero=int(input("Numéro invalide, Recommencez s'il vous plaît : "))
    grille[ligne-1][colonne-1]=numero
    return grille

# Verifier si toutes les cases du sudoku sont remplies
def grille_finie(g):
    n=len(g)
    nbr_cases_vides=0
    for i in range(n):
        for j in range(n):
            if g[i][j]==0:
                nbr_cases_vides+=1
    if nbr_cases_vides==0:
        return True
    return False

#Verifie si le sudoku est gagnant
def grille_gagnante(g):
    n=len(g) #normalement n=9
    for i in range(n):
        for j in range(n):
            for k in range(n):
                if g[i][j]==g[i][k]  and k!=j :
                    return False
    for j in range(n):
        for i in range(n):
            for l in range(n):
                if g[i][j]==g[l][j] and l!=i :
                    return False

    for a in range(0,7,3):
        for i in range(3):
            for j in range(3):
                for l in range(3) :
                    for k in range(3):
                        if g[i+a][j+a]==g[l+a][k+a]  and l!=i and k!=j:
                            return False
    return True

#Jeu Final prend les commandes du joueur
def play_sudoku(grille):
    print(affiche(grille))
    global pos_initiales
    pos_initiales = positions_initiales(grille)
    while not grille_finie(grille) :
        grille0=grille
        grille=play_sudoku_etape1(grille0)
        print(affiche(grille))
    if grille_gagnante(grille):
        print ("Gagné")
    else :
        print("Perdu")


import random as rd
#Jeu Final joue aleatoirement
def random_play_sudoku(grille):
    pos_initiales=positions_initiales(grille)
    while grille_finie(grille) == False:
        for i in range(9):
            for j in range(9):
                if [i,j] not in pos_initiales :
                    grille[i][j]=rd.randint(1,9)
    print(affiche(grille))
    return grille_gagnante(grille)

#Interface graphique

def liste_de_trois(g):
    l=[]
    for a in range(0,7,3):
        for b in range(0,7,3):
            l1=[]
            for i in range(3):
                l2=[]
                for j in range(3):
                    l2.append(g[i+a][j+b])
                l1.append(l2)
            l.append(l1)
    return (l)


from tkinter import *

def graphical_sudoku_vide():
    grandes_cases=[]
    root = Tk()
    root.title("sudoku")
    top=Toplevel(root)
    background = Frame(top)
    background.grid(row=0,column=0)
    for i in range(3):
        grandes_cases.append([])
        for j in range(3):
            grandes_cases[i].append(Frame(background, bg="#eee4da", height=180, width=180,bd=2,relief='groove'))
            grandes_cases[i][j].grid(row=i,column=j)
            petites_cases=[]
            for k in range(3):
                petites_cases.append([])
                for l in range(3):
                    petites_cases[k].append(Frame(grandes_cases[i][j],bg="#eee4da", height=60, width=60,bd=1,relief='groove' ))
                    petites_cases[k][l].grid(row=k,column=l)
    root.mainloop()

def graphical_sudoku_init(grille_init):
    grandes_cases=[]
    root = Tk()
    root.title("sudoku")
    top=Toplevel(root)
    background = Frame(top)
    background.grid(row=0,column=0)
    for i in range(3):
        grandes_cases.append([])
        for j in range(3):
            grandes_cases[i].append(Frame(background, bg="#eee4da", height=180, width=180,bd=2,relief='groove'))
            grandes_cases[i][j].grid(row=i,column=j)
            petites_cases=[]
            for k in range(3):
                petites_cases.append([])
                for l in range(3):
                    petites_cases[k].append(Frame(grandes_cases[i][j],bg="#eee4da", height=60, width=60,bd=1,relief='groove' ))
                    petites_cases[k][l].grid(row=k,column=l)
                    if grille_init[3*i + j][k][l] !=0 :
                        text=Label(petites_cases[k][l], text=str(grille_init[3*i + j][k][l]), bg="#eee4da", fg="#776e65")
                        text.pack()
    root.mainloop()


#Essais

def reciproque_liste_de_trois(grille):
    grille9=[]
    for i in range(3):
        grille9.append(grille[0][i%3]+grille[1][i%3]+grille[2][i%3])
    for i in range(3,6):
        grille9.append(grille[3][i%3]+grille[4][i%3]+grille[5][i%3])
    for i in range(6,9):
        grille9.append(grille[6][i%3]+grille[7][i%3]+grille[8][i%3])
    return grille9


grille_facile = [[0, 0, 7, 9, 2, 0, 0, 0, 0], [0, 3, 8, 0, 6, 7, 0, 0, 0], [0, 0, 0, 0, 0, 3, 5, 0, 7], [8, 0, 2, 0, 0, 6, 7, 0, 1], [0, 5, 0, 0, 1, 0, 0, 6, 0], [7, 0, 1, 2, 0, 0, 9, 0, 4], [9, 0, 3, 8, 0, 0, 0, 0, 0], [0, 0, 0, 6, 3, 0, 2, 9, 0], [0, 0, 0, 0, 9, 1, 4, 0, 0]]
grille_valable=[[3,2,7,9,5,6,8,1,4],[4,5,8,1,7,3,9,2,6],[1,9,6,8,4,2,7,5,3],[5,8,9,7,6,1,4,3,2],[2,4,3,5,8,9,6,7,1],[7,6,1,2,3,4,5,9,8],[6,1,4,3,9,5,2,8,7],[8,3,5,4,2,7,1,6,9],[9,7,2,6,1,8,3,4,5]]
grille_valable_initiale =[[3,2,7,9,5,6,8,1,4],[4,5,8,1,7,3,0,2,6],[0,9,6,8,4,2,7,5,3],[5,8,9,7,6,1,4,3,2],[2,4,3,5,8,9,6,7,1],[7,6,1,2,3,4,5,9,8],[6,1,0,3,9,5,2,8,7],[8,3,5,4,2,7,1,6,9],[9,0,2,6,1,8,3,4,5]]

grille_init = liste_de_trois(grille_valable_initiale)

grandes_cases=[]
root=Tk()
root.title("sudoku")
top=Toplevel(root)
background = Frame(top)
background.grid(row=0,column=0)
global entrees, values
entrees=liste_de_trois([[0]*9]*9)
values = liste_de_trois([[0]*9]*9)
for i in range(3):
    grandes_cases.append([])
    for j in range(3):
        grandes_cases[i].append(Frame(background, bg="#eee4da", height=300, width=300,bd=2,relief='groove'))
        grandes_cases[i][j].grid(row=i,column=j)
        petites_cases=[]
        for k in range(3):
            petites_cases.append([])
            for l in range(3):
                petites_cases[k].append(Frame(grandes_cases[i][j],bg="#eee4da", height=100, width=100,bd=1,relief='groove' ))
                petites_cases[k][l].grid(row=k,column=l)
                if grille_init[3*i + j][k][l] != 0 :
                    text=Label(petites_cases[k][l], text=str(grille_init[3*i + j][k][l]), bg="#eee4da", fg="#776e65", width=10, height=4)
                    text.pack()
                elif grille_init[3*i + j][k][l] == 0 :
                    values[3*i + j][k][l] = StringVar()
                    entrees[3*i + j][k][l] = Entry(petites_cases[k][l], textvariable=values[3*i + j][k][l], width=10)
                    entrees[3*i + j][k][l].pack()

def recupere():
    global grille
    grille=grille_init[:]
    E=0
    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    if grille_init[3*i + j][k][l]==0:
                        if entrees[3*i + j][k][l].get()=='':
                            print('Attention vous avez des cases vides !')
                        else :
                            grille[3*i + j][k][l]=int(entrees[3*i + j][k][l].get())
    print(grille)
    fenetre_finale=Tk()
    fenetre_finale.title("Résultat")
    grille9=reciproque_liste_de_trois(grille)
    if grille_gagnante(grille9):
        label = Label(fenetre_finale, text="Vous avez gagné !")
        label.pack()
    else :
        label = Label(fenetre_finale, text="Vous avez perdu !")
        label.pack()
    fenetre_finale.mainloop()

bouton = Button(root, text="Valider", command=recupere)
bouton.pack()
root.mainloop()





#fin du jeu
